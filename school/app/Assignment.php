<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assignment extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['title','content','status','startdate','enddate','priority'];
    public function classroom()
    {
        return $this->belongsTo('App\Classroom');
    }
}
