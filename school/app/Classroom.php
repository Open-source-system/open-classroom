<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classroom extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['name','description','status'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function blog()
    {
        return $this->hasMany('App\Blog');
    }
    public function assignment()
    {
        return $this->hasMany('App\Assignment');
    }
}
