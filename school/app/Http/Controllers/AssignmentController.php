<?php namespace App\Http\Controllers;
use App\Classroom;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Assignment;
use Illuminate\Http\Request;
use App\Policies\AssignmentPolicy;

class AssignmentController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('throttle:30,1');
	}
	public function index($classroomid)
	{
		$assignments = Assignment::orderBy('id', 'desc')->paginate(10);

		return view('assignments.index', compact('assignments','classroomid'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($classroomid)
	{
		return view('assignments.create',compact('classroomid'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store($classroomid,Request $request)
	{

		$this->validate($request, [
				'title' => 'required|max:255',
				'content' => 'required|max:65000',
				'status' => 'required|between:0,1',
				'startdate' => 'date|after:enddate',
				'enddate' => 'date|after:startdate',
				'priority' => 'required|between:0,2',
		]);
		$assignment = new Assignment();

		$assignment->title = $request->input("title");
        $assignment->content = $request->input("content");
        $assignment->status = $request->input("status");
        $assignment->startdate = $request->input("startdate");
        $assignment->enddate = $request->input("enddate");
        $assignment->priority = $request->input("priority");
		$assignment->classroom_id=$classroomid;
		$assignment->user_id=Auth::user()->id;
		$this->authorize('create',$assignment);
		$assignment->save();

		return redirect()->route('classrooms.assignments.index',compact('classroomid'))->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($classroomid,$id)
	{
		$assignment = Assignment::findOrFail($id);

		return view('assignments.show', compact('assignment','classroomid'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($classroomid,$id)
	{
		$assignment = Assignment::findOrFail($id);
		$this->authorize('update', $assignment);
		return view('assignments.edit', compact('assignment','classroomid'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update($classroomid,Request $request, $id)
	{
		$this->validate($request, [
				'title' => 'required|max:255',
				'content' => 'required|max:65000',
				'status' => 'required|between:0,1',
				'startdate' => 'date|after:enddate',
				'enddate' => 'date|after:startdate',
				'priority' => 'required|between:0,2',
		]);
		$assignment = Assignment::findOrFail($id);
		$this->authorize('update', $assignment);
		$assignment->title = $request->input("title");
        $assignment->content = $request->input("content");
        $assignment->status = $request->input("status");
        $assignment->startdate = $request->input("startdate");
        $assignment->enddate = $request->input("enddate");
        $assignment->priority = $request->input("priority");

		$assignment->save();

		return redirect()->route('classrooms.assignments.index',compact('classroomid'))->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($classroomid,$id)
	{
		$assignment = Assignment::findOrFail($id);
		$this->authorize('delete', $assignment);
		$assignment->delete();

		return redirect()->route('classrooms.assignments.index',compact('classroomid'))->with('message', 'Item deleted successfully.');
	}

}
