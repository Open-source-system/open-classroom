<?php namespace App\Http\Controllers;

use App\Classroom;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Mail;
use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Policies\BlogPolicy;

class BlogController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('throttle:30,1');
	}
	public function index($classroomid)
	{
		$blogs = Blog::orderBy('id', 'desc')->paginate(10);
		return view('blogs.index', compact('blogs','classroomid'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($classroomid)
	{

		return view('blogs.create',compact('classroomid'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store($classroomid,Request $request)
	{

		$this->validate($request, [
				'title' => 'required|max:255',
				'content' => 'required|max:65000',
				'status' => 'required|between:0,1',
		]);
		$blog = new Blog();

		$blog->title = $request->input("title");
        $blog->content = $request->input("content");
        $blog->status = $request->input("status");
		$blog->classroom_id=$classroomid;
		$blog->user_id=Auth::user()->id;
		$this->authorize('create',$blog);
		$blog->save();
		Mail::queue('emails.welcome', compact($blog), function ($message) {
			$message->from('vaidhyanathan93@hotmail.com', 'openclassrooms');
			$message->to('vaidhyanathan93@gmail.com');
		});
		return redirect()->route('classrooms.blogs.index',compact('classroomid'))->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($classroomid,$id)
	{
		$blog = Blog::findOrFail($id);

		return view('blogs.show', compact('blog','classroomid'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($classroomid,$id)
	{
		$blog = Blog::findOrFail($id);
		$this->authorize('update', $blog);
		return view('blogs.edit', compact('blog','classroomid'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update($classroomid,Request $request, $id)
	{
		$this->validate($request, [
				'title' => 'required|max:255',
				'content' => 'required|max:65000',
				'status' => 'required|between:0,1',
		]);
		$blog = Blog::findOrFail($id);
		$this->authorize('update', $blog);
		$blog->title = $request->input("title");
        $blog->content = $request->input("content");
        $blog->status = $request->input("status");

		$blog->save();

		return redirect()->route('classrooms.blogs.index',compact('classroomid'))->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($classroomid,$id)
	{
		$blog = Blog::findOrFail($id);
		$this->authorize('delete', $blog);
		$blog->delete();

		return redirect()->route('classrooms.blogs.index',compact('classroomid'))->with('message', 'Item deleted successfully.');
	}

}
