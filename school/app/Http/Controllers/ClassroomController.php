<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Classroom;
use Illuminate\Http\Request;
use Gate;
use Illuminate\Support\Facades\Auth;

class ClassroomController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('throttle:30,1');
	}

	public function index()
	{
		$classrooms = Classroom::orderBy('id', 'desc')->paginate(10);

		return view('classrooms.index', compact('classrooms'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('classrooms.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
				'name' => 'required|max:255',
				'description' => 'required|max:2000',
				'status' => 'required|between:0,1'
		]);
		$classroom = new Classroom();

		$classroom->name = $request->input("name");
        $classroom->description = $request->input("description");
        $classroom->status = $request->input("status");
		$request->user()->classroom()->save($classroom);
		return redirect()->route('classrooms.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$classroom = Classroom::findOrFail($id);

		return view('classrooms.show', compact('classroom'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		$classroom = Classroom::findOrFail($id);

		$this->authorize('update', $classroom);
		return view('classrooms.edit', compact('classroom'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
				'name' => 'required|max:255',
				'description' => 'required|max:2000',
				'status' => 'required|between:0,1'
		]);
		$classroom = Classroom::findOrFail($id);

		$this->authorize('update', $classroom);
		$classroom->name = $request->input("name");
        $classroom->description = $request->input("description");
        $classroom->status = $request->input("status");

		$classroom->save();

		return redirect()->route('classrooms.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$classroom = Classroom::findOrFail($id);
		$this->authorize('delete', $classroom);
		$classroom->delete();

		return redirect()->route('classrooms.index')->with('message', 'Item deleted successfully.');
	}

}
