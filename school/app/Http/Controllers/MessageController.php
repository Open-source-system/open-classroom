<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\Policies\MessagePolicy;
use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('throttle:30,1');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index($classroomid)
	{
		$messages = Message::orderBy('id', 'desc')->paginate(10);

		return view('messages.index', compact('messages','classroomid'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($classroomid)
	{
		return view('messages.create',compact('classroomid'));
	}

	/**
	 * Store a newly created resource in storage.
	 *s
	 * @param Request $request
	 * @return Response
	 */
	public function store($classroomid,Request $request)
	{
		$this->validate($request, [
				'title' => 'required|max:255',
				'content' => 'required|max:2000',
				'status' => 'required|between:0,1',
				'to' => 'exists:users,id',
		]);
		$message = new Message();

		$message->title = $request->input("title");
        $message->content = $request->input("content");
        $message->status = $request->input("status");
        $message->to = $request->input("to");
		$message->classroom_id=$classroomid;
		$message->user_id=Auth::user()->id;
		$this->authorize('create',$message);
		$message->save();

		return redirect()->route('classrooms.messages.index',compact('classroomid'))->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($classroomid,$id)
	{
		$message = Message::findOrFail($id);

		return view('messages.show', compact('message','classroomid'));
	}
	public function destroy($classroomid,$id)
	{
		$message = Message::findOrFail($id);
		$this->authorize('delete', $message);
		$message->delete();

		return redirect()->route('classrooms.messages.index','classroomid')->with('message', 'Item deleted successfully.');
	}

}
