<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Video;
use Illuminate\Http\Request;

class VideoController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('throttle:30,1');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$videos = Video::orderBy('id', 'desc')->paginate(10);

		return view('videos.index', compact('videos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('videos.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
				'title' => 'required|max:255',
				'description' => 'required|max:2000',
				'status' => 'required|between:0,1',
		]);
		$video = new Video();

		$video->title = $request->input("title");
        $video->description = $request->input("description");
        $video->status = $request->input("status");
        $video->location = $request->input("location");

		$video->save();

		return redirect()->route('videos.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$video = Video::findOrFail($id);

		return view('videos.show', compact('video'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		$video = Video::findOrFail($id);
		$this->authorize('update', $video);
		return view('videos.edit', compact('video'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
				'title' => 'required|max:255',
				'description' => 'required|max:2000',
				'status' => 'required|between:0,1',
		]);
		$video = Video::findOrFail($id);
		$this->authorize('update', $video);
		$video->title = $request->input("title");
        $video->description = $request->input("description");
        $video->status = $request->input("status");

		$video->save();

		return redirect()->route('videos.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$video = Video::findOrFail($id);
		$this->authorize('delete', $video);
		$video->delete();

		return redirect()->route('videos.index')->with('message', 'Item deleted successfully.');
	}

}
