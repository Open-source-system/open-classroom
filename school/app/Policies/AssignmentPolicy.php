<?php

namespace App\Policies;

use App\Assignment;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
class AssignmentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function before($user, $ability)
    {
//        if ($user->isSuperAdmin()) {
//            return true;
//        }
    }
    public function create(User $user, Assignment $assignment)
    {
        return true;
    }
    public function update(User $user, Assignment $assignment)
    {
        return $user->id == $assignment->user_id;
    }
    public function delete(User $user, Assignment $assignment)
    {
        return $user->id == $assignment->user_id;
    }
}
