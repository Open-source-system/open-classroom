<?php

namespace App\Policies;

use App\Classroom;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;

class ClassroomPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function before($user, $ability)
    {
//        if ($user->isSuperAdmin()) {
//            return true;
//        }
    }
    public function update(User $user, Classroom $classroom)
    {
        return $user->id == $classroom->user_id;
    }
    public function delete(User $user, Classroom $classroom)
    {
        return $user->id == $classroom->user_id;
    }
}
