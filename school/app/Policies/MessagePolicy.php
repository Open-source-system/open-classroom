<?php

namespace App\Policies;

use App\Message;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MessagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function before($user, $ability)
    {
//        if ($user->isSuperAdmin()) {
//            return true;
//        }
    }
    public function create(User $user, Message $message)
    {
        return true;
    }
    public function update(User $user, Message $message)
    {
        return $user->id == $message->user_id;
    }
    public function delete(User $user, Message $message)
    {
        return $user->id == $message->user_id;
    }
}
