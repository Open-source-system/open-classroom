<?php

namespace App\Policies;

use App\Video;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;

class VideoPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function update(User $user, Video $video)
    {
        return $user->id == $video->user_id;
    }
    public function delete(User $user, Video $video)
    {
        return $user->id == $video->user_id;
    }
}
