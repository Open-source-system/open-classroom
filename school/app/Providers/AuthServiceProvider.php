<?php

namespace App\Providers;

use App\Assignment;
use App\Blog;
use App\Classroom;
use App\Comment;
use App\Message;
use App\Policies\AssignmentPolicy;
use App\Policies\BlogPolicy;
use App\Policies\CommentPolicy;
use App\Policies\MessagePolicy;
use App\Policies\VideoPolicy;
use App\Video;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Policies\ClassroomPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Classroom::class => ClassroomPolicy::class,
        Assignment::class => AssignmentPolicy::class,
        Blog::class => BlogPolicy::class,
        Comment::class => CommentPolicy::class,
        Message::class =>MessagePolicy::class,
        Video::class => VideoPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //
    }
}
