<?php
/**
 * Created by PhpStorm.
 * User: vaidhy-3414
 * Date: 6/4/16
 * Time: 10:29 PM
 */

namespace App\Repositories;

use App\User;
use App\Classroom;

class ClassRoomRepository
{
    /**
     * Get all of the tasks for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    public function forUser(User $user)
    {
        return Classroom::where('user_id', $user->id)
            ->orderBy('created_at', 'asc')
            ->get();
    }
}