<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('assignments', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title')->default('');
            $table->longText('content');
            $table->integer('status');
            $table->dateTime('startdate');
            $table->dateTime('enddate');
            $table->integer('priority');
			$table->integer('user_id')->index();
			$table->integer('classroom_id')->index();
			$table->timestamps();
			$table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('assignments');
	}

}
