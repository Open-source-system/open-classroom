@extends('layout')

@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Assignments / Edit #{{$assignment->id}}</h1>
    </div>
@endsection

@section('content')
    @include('common.errors')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('classrooms.assignments.update',[$classroomid, $assignment->id]) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('title')) has-error @endif">
                       <label for="title-field">Title</label>
                    <input type="text" id="title-field" name="title" class="form-control" value="{{ $assignment->title }}"/>
                       @if($errors->has("title"))
                        <span class="help-block">{{ $errors->first("title") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('content')) has-error @endif">
                       <label for="content-field">Content</label>
                    <input type="text" id="content-field" name="content" class="form-control" value="{{ $assignment->content }}"/>
                       @if($errors->has("content"))
                        <span class="help-block">{{ $errors->first("content") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('status')) has-error @endif">
                       <label for="status-field">Status</label>
                    <input type="text" id="status-field" name="status" class="form-control" value="{{ $assignment->status }}"/>
                       @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('startdate')) has-error @endif">
                       <label for="startdate-field">Startdate</label>
                    <input type="text" id="startdate-field" name="startdate" class="form-control" value="{{ $assignment->startdate }}"/>
                       @if($errors->has("startdate"))
                        <span class="help-block">{{ $errors->first("startdate") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('enddate')) has-error @endif">
                       <label for="enddate-field">Enddate</label>
                    <input type="text" id="enddate-field" name="enddate" class="form-control" value="{{ $assignment->enddate }}"/>
                       @if($errors->has("enddate"))
                        <span class="help-block">{{ $errors->first("enddate") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('priority')) has-error @endif">
                       <label for="priority-field">Priority</label>
                    <input type="text" id="priority-field" name="priority" class="form-control" value="{{ $assignment->priority }}"/>
                       @if($errors->has("priority"))
                        <span class="help-block">{{ $errors->first("priority") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('classrooms.assignments.index',$classroomid) }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection