@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Assignments
            <a class="btn btn-success pull-right" href="{{ route('classrooms.assignments.create',$classroomid) }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($assignments->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>TITLE</th>
                        <th>CONTENT</th>
                        <th>STATUS</th>
                        <th>STARTDATE</th>
                        <th>ENDDATE</th>
                        <th>PRIORITY</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($assignments as $assignment)
                            <tr>
                                <td>{{$assignment->id}}</td>
                                <td>{{$assignment->title}}</td>
                    <td>{{$assignment->content}}</td>
                    <td>{{$assignment->status}}</td>
                    <td>{{$assignment->startdate}}</td>
                    <td>{{$assignment->enddate}}</td>
                    <td>{{$assignment->priority}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('classrooms.assignments.show',[$classroomid, $assignment->id]) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    @can('update', $assignment)
                                        <a class="btn btn-xs btn-warning" href="{{ route('classrooms.assignments.edit',[$classroomid, $assignment->id]) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    @endcan
                                    @can('delete',$assignment)
                                    <form action="{{ route('classrooms.assignments.destroy', [$classroomid,$assignment->id]) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $assignments->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection