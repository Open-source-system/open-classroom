@extends('layout')
@section('header')
<div class="page-header">
        <h1>Assignments / Show #{{$assignment->id}}</h1>
        <form action="{{ route('classrooms.assignments.destroy',[$classroomid, $assignment->id]) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('classrooms.assignments.edit', [$classroomid,$assignment->id]) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="title">TITLE</label>
                     <p class="form-control-static">{{$assignment->title}}</p>
                </div>
                    <div class="form-group">
                     <label for="content">CONTENT</label>
                     <p class="form-control-static">{{$assignment->content}}</p>
                </div>
                    <div class="form-group">
                     <label for="status">STATUS</label>
                     <p class="form-control-static">{{$assignment->status}}</p>
                </div>
                    <div class="form-group">
                     <label for="startdate">STARTDATE</label>
                     <p class="form-control-static">{{$assignment->startdate}}</p>
                </div>
                    <div class="form-group">
                     <label for="enddate">ENDDATE</label>
                     <p class="form-control-static">{{$assignment->enddate}}</p>
                </div>
                    <div class="form-group">
                     <label for="priority">PRIORITY</label>
                     <p class="form-control-static">{{$assignment->priority}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('classrooms.assignments.index',$classroomid) }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection