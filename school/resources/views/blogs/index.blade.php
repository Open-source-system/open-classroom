@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Blogs
            <a class="btn btn-success pull-right" href="{{ route('classrooms.blogs.create',$classroomid) }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($blogs->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>TITLE</th>
                        <th>CONTENT</th>
                        <th>STATUS</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($blogs as $blog)
                            <tr>
                                <td>{{$blog->id}}</td>
                                <td>{{$blog->title}}</td>
                    <td>{{$blog->content}}</td>
                    <td>{{$blog->status}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('classrooms.blogs.show',[ $classroomid,$blog->id]) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    @can('update', $blog)
                                    <a class="btn btn-xs btn-warning" href="{{ route('classrooms.blogs.edit',[$classroomid, $blog->id]) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    @endcan
                                    @can('delete', $blog)
                                    <form action="{{ route('classrooms.blogs.destroy',[$classroomid, $blog->id]) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $blogs->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection