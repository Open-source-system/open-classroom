@extends('layout')
@section('header')
<div class="page-header">
        <h1>Blogs / Show #{{$blog->id}}</h1>
    @can('delete', $blog)
        <form action="{{ route('classrooms.blogs.destroy', [$classroomid,$blog->id]) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">

                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    @endcan
    @can('update', $blog)
    <a class="btn btn-warning btn-group" role="group" href="{{ route('classrooms.blogs.edit',[$classroomid, $blog->id]) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
    @endcan
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="title">TITLE</label>
                     <p class="form-control-static">{{$blog->title}}</p>
                </div>
                    <div class="form-group">
                     <label for="content">CONTENT</label>
                     <p class="form-control-static">{{$blog->content}}</p>
                </div>
                    <div class="form-group">
                     <label for="status">STATUS</label>
                     <p class="form-control-static">{{$blog->status}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('classrooms.blogs.index',$classroomid) }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection