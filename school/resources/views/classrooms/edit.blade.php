@extends('layout')

@section('header')
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{ route('classrooms.index') }}"><i class="fa fa-dashboard"></i> Classroom</a></li>
            <li class="active">Edit</li>
        </ol>
        <div class="page-header">
            Edit classroom
        </div>
    </section>
@endsection

@section('content')
    @include('common.errors')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">

                    <!-- form start -->
                    <form action="{{ route('classrooms.update', $classroom->id) }}" method="POST">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="box-body">

                            <div class="form-group @if($errors->has('name')) has-error @endif">
                                <label for="name-field">Name</label>
                                <input type="text" id="name-field" name="name" class="form-control" value="{{ $classroom->name }}"/>
                                @if($errors->has("name"))
                                    <span class="help-block">{{ $errors->first("name") }}</span>
                                @endif
                            </div>
                            <div class="form-group @if($errors->has('description')) has-error @endif">
                                <label for="description-field">Description</label>
                                <textarea class="form-control textarea" id="description-field" rows="3" name="description">{{ $classroom->description }}</textarea>
                                @if($errors->has("description"))
                                    <span class="help-block">{{ $errors->first("description") }}</span>
                                @endif
                            </div>
                            <div class="form-group @if($errors->has('status')) has-error @endif">
                                <label for="status-field">Status</label>
                                <input type="text" id="status-field" name="status" class="form-control" value="{{ $classroom->status }}"/>
                                @if($errors->has("status"))
                                    <span class="help-block">{{ $errors->first("status") }}</span>
                                @endif
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a class="btn btn-link pull-right" href="{{ route('classrooms.index') }}">Back</a>

                        </div>
                    </form>
                </div>

            </div>
            <!--/.col (right) -->
        </div>   <!-- /.row -->
    </section>
@endsection