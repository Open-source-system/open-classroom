@extends('layout')

@section('header')
    <section class="content-header">
        <h1>
            Classroom
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

@endsection

@section('content')
    <div class="row">
            @if($classrooms->count())
                        @foreach($classrooms as $classroom)
                <div class="col-md-4 col-sm-6 col-xs-12 col-lg-4">
                    @if($classroom->status==1)
                    <div class="panel panel-default">
                        @elseif($classroom->status==2)
                            <div class="panel panel-warning">
                        @endif

                        <div class="panel-heading">
                            <h3 class="panel-title">{{$classroom->name}}</h3>
                        </div>
                        <div class="panel-body">
                            {!! $classroom->description!!}
                        </div>
                    </div>
                            {{--<tr>--}}
                                {{--<td>{{$classroom->id}}</td>--}}
                                {{--<td></td>--}}
                    {{--<td></td>--}}
                    {{--<td>{{$classroom->status}}</td>--}}
                                {{--<td class="text-right">--}}
                                    {{--<a class="btn btn-xs btn-primary" href="{{ route('classrooms.show', $classroom->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>--}}
                            @can('update', $classroom)
                            {{--<a class="btn btn-xs btn-warning" href="{{ route('classrooms.edit', $classroom->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>--}}
                            @endcan
                            @can('delete', $classroom)
                                    {{--<form action="{{ route('classrooms.destroy', $classroom->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">--}}
                                        {{--<input type="hidden" name="_method" value="DELETE">--}}
                                        {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                                        {{--<button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>--}}
                                    {{--</form>--}}
                            @endcan
                                {{--</td>--}}
                            {{--</tr>--}}
                            </div>
                        @endforeach
                {!! $classrooms->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

    </div>

@endsection
</div>