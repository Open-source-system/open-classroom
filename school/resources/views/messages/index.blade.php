@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Messages
            <a class="btn btn-success pull-right" href="{{ route('classrooms.messages.create',$classroomid) }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($messages->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>TITLE</th>
                        <th>CONTENT</th>
                        <th>STATUS</th>
                        <th>TO</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($messages as $message)
                            <tr>
                                <td>{{$message->id}}</td>
                                <td>{{$message->title}}</td>
                    <td>{{$message->content}}</td>
                    <td>{{$message->status}}</td>
                    <td>{{$message->to}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('classrooms.messages.show',[$classroomid, $message->id]) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    @can('update', $message)
                                    <a class="btn btn-xs btn-warning" href="{{ route('classrooms.messages.edit', [$classroomid,$message->id]) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                   @endcan
                                    @can('delete', $message)
                                    <form action="{{ route('classrooms.messages.destroy',[$classroomid ,$message->id]) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $messages->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection