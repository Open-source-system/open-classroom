@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Videos
            <a class="btn btn-success pull-right" href="{{ route('videos.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($videos->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>TITLE</th>
                        <th>DESCRIPTION</th>
                        <th>STATUS</th>
                        <th>LOCATION</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($videos as $video)
                            <tr>
                                <td>{{$video->id}}</td>
                                <td>{{$video->title}}</td>
                    <td>{{$video->description}}</td>
                    <td>{{$video->status}}</td>
                    <td>{{$video->location}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('videos.show', $video->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    @can('update', $video)
                                    <a class="btn btn-xs btn-warning" href="{{ route('videos.edit', $video->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    @endcan
                                    @can('delete', $video)
                                    <form action="{{ route('videos.destroy', $video->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $videos->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection